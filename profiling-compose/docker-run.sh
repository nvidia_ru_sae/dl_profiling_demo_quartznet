#!/usr/bin/env bash

[[ -z $1 ]] && echo "specify the executable name" && exit 1
PARAMETERS="${2}"
if [[ $1 == "dlprof" ]]; then
   [[ -z $2 ]] && echo "specify the run name" && exit 2
   RUN=`basename ${2}`
   PARAMETERS="--nsys_database /workspace/ext/profiles/${RUN}/nsys_profile.sqlite"
fi
CUR_DIR=`dirname "$0"`/
PARENT_DIR=`realpath "$CUR_DIR/../"`

echo $PARENT_DIR

docker run --rm \
           --runtime runc \
           --gpus device=GPU-865a105d-ff60-cd2c-bdbc-99c009b7e561 \
            --env HISTFILE=/ext_workspace/docker/unix/docker_bash_history \
            --env HISTFILESIZE="" \
            --env HISTSIZE="" \
           --expose 6006/tcp \
           --expose 8787/tcp \
           --expose 8888/tcp \
           --interactive \
           --ipc host \
           --name dlprof_view \
           --publish 6096:6006/tcp \
           --publish 8797:8787/tcp \
           --publish 9998:8888/tcp \
           --security-opt label=disable \
           --shm-size 1G \
           --tty \
           --volume ${PARENT_DIR}/NeMo:/workspace/nemo:rw \
           --volume ${PARENT_DIR}/:/workspace/ext:rw \
           --volume /home/dmitrym/workspace_ru:/ext_workspace:rw \
           --volume /home/dmitrym:/ext_home:rw \
           --volume /mnt/hdd/datasets:/datasets:rw \
           --volume /mnt/hdd/results/nemo:/results:rw \
           --volume /tmp/.X11-unix:/tmp/.X11-unix:rw \
           --workdir /workspace \
           nvcr.io/nvidian/dmitrym_nemo_20.07:latest \
           /bin/bash -c "source ~/.bashrc && nvidia-smi -L && ${1} ${PARAMETERS}"
        #    /bin/bash -c "source ~/.bashrc && CUDA_VISIBLE_DEVICES='GPU-865a105d-ff60-cd2c-bdbc-99c009b7e561' jupyter lab --ip=0.0.0.0 --allow-root --no-browser --NotebookApp.token='vscode_demo' --NotebookApp.allow_origin='*' --notebook-dir=/"
        #    nvidia-smi
        #    nsys status -e 
        #    --privileged \
        #    --cap-add=SYS_ADMIN \
        #    --net profiling-compose_default \
