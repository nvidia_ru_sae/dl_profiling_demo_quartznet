
ARG FROM_IMAGE
FROM ${FROM_IMAGE}

RUN  /bin/bash -c "echo apt-get: installing htop \
  && apt-get update && apt-get install -y \
    build-essential \
    htop \
  && rm -rf /var/lib/apt/lists/* \
"
RUN  /bin/bash -c "echo preparing the history saving \
  && echo '' >> $HOME/.bashrc \ 
  && cat $HOME/.bashrc | grep -v HIST > /tmp/.bashrc \ 
  && mv /tmp/.bashrc $HOME/.bashrc \
  && source $HOME/.bashrc \
  "

RUN  /bin/bash -c "echo pip: installing pylint \
  # && export PATH=/opt/conda/envs/rapids/bin:$PATH \
  # && pip install pylint \
  " 

RUN  /bin/bash -c "echo conda: installing jupyter lab \
  && source ~/.bashrc \
  # && conda install -n rapids -c conda-forge jupyterlab nodejs \
  && conda install -c conda-forge jupyterlab nodejs'>=10.0.0' pylint \
  "

RUN  /bin/bash -c "echo 'jupyter lab code navigation (requires nodejs)' \
  && source ~/.bashrc \
  # && export PATH=/opt/conda/envs/rapids/bin:$PATH \
  && jupyter labextension install @krassowski/jupyterlab_go_to_definition \
  && mkdir -p  /workspace/.jupyter/lab/user-settings/\@krassowski/jupyterlab_go_to_definition \
  && echo '{\"modifier\":\"Shift\"}' > /workspace/.jupyter/lab/user-settings/\@krassowski/jupyterlab_go_to_definition/plugin.jupyterlab-settings \
  "

CMD /bin/bash -c "source ~/.bashrc && jupyter lab --ip=0.0.0.0 --allow-root --no-browser --NotebookApp.token='demotoken' --NotebookApp.allow_origin='*' --notebook-dir=/ext_workspace/code/dl_profiling_demo_quartznet"