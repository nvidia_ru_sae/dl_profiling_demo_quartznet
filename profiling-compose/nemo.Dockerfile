
ARG FROM_IMAGE
FROM ${FROM_IMAGE}

# COPY requirements/requirements_docker.txt nemo_requirements.txt 

# RUN  /bin/bash -c "echo pip: installing requirements \
#   # && export PATH=/opt/conda/envs/rapids/bin:$PATH \
#   && pip install --disable-pip-version-check --no-cache-dir -r nemo_requirements.txt \
#   " 

# COPY reinstall.sh nemo_install.sh
# RUN  /bin/bash -c "echo pip: installing requirements \
#   # && export PATH=/opt/conda/envs/rapids/bin:$PATH \
#   && ./nemo_install.sh \

RUN  /bin/bash -c "echo apt-get: installing libsndfile1 ffmpeg \
  && apt-get update && apt-get install -y \
    libsndfile1 \
    ffmpeg \
    sox \
    libcurl4-openssl-dev \
    libsox-dev \
  && rm -rf /var/lib/apt/lists/* \
"

RUN  /bin/bash -c "echo pip: installing nemo \
  # && export PATH=/opt/conda/envs/rapids/bin:$PATH \
  && pip install Cython \
  && pip install nemo_toolkit[all] nemo-asr \
  # && pip install git+git://github.com/pytorch/audio.git@82e02e85336 \
  && pip install --no-deps torchaudio==0.5.0 \
  && pip install git+https://github.com/NVIDIA/PyProf@master \
  " 

# RUN  /bin/bash -c "echo conda: installing torchaudio torchvision \
#   && source ~/.bashrc \
#   && conda install -c pytorch torchaudio torchvision \
#   "