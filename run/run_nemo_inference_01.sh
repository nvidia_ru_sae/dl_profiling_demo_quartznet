#!/usr/bin/env bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup

python ./01_speech2text_infer.py $INFERENCE_ARGS | tee -a $INFERENCE_LOG

