#!/bin/bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup
cd /workspace/jasper

set -x
nsys profile $NSYS_ARGS --delay=20 --duration=40 python tensorrt/perf.py --use_existing_engine --batch_size 64 --engine_batch_size 64 --model_toml configs/jasper10x5dr_nomask.toml --dataset_dir /datasets/LibriSpeech --val_manifest /datasets/LibriSpeech/librispeech-dev-clean-wav.json --ckpt_path /checkpoints/jasper_fp16.pt --trt_fp16 --pyt_fp16 --num_steps 100 --engine_path /results/engines/fp16_DYNAMIC.engine --onnx_path /results/onnxs/fp32_DYNAMIC.onnx --seq_len 512 --use_existing_engine --csv_path /results/res.csv | tee -a $INFERENCE_LOG

# dlprof $DLPROF_ARGS | tee -a $INFERENCE_LOG # dlprof is not activated neither in TRT, nor in PyTorch

cp -r ${DLPROF_TMP}* $RUN_DIR/

set +x