#!/bin/bash 

LOGDIR="${1}/event_files"
[[ ! -d  "${LOGDIR}" ]] && echo "Usage: ${0} <path to directory, containing event_files subdirectory>" && exit 1

tensorboard --logdir "${LOGDIR}"