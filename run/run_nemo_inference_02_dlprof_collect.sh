#!/bin/bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup
# this node name is found via https://docs.nvidia.com/deeplearning/frameworks/dlprof-user-guide/#find_good_key_node

DLPROF_ARGS+=" --key_node /main/NeuralModuleFactory::infer/PtActions::infer/PtActions::_infer/DataLoader::new_iter/DataLoader/DataLoader " 

set -x
# nsys profile $NSYS_ARGS python "${CUR_DIR}./02_speech2text_infer_pyprof.py" $INFERENCE_ARGS | tee -a $INFERENCE_LOG

dlprof $DLPROF_ARGS | tee -a $INFERENCE_LOG

cp -r ${DLPROF_TMP}* $RUN_DIR/

set +x