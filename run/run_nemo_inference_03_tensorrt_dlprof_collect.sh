#!/bin/bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup

DLPROF_ARGS+=" --key_node /main/NeuralModuleFactory::infer/PtActions::infer/PtActions::_infer/DataLoader::new_iter/DataLoader/DataLoader " 

set -x
nsys profile $NSYS_ARGS python "${CUR_DIR}./03_speech2text_infer_tensorrt.py" $INFERENCE_ARGS | tee -a $INFERENCE_LOG
dlprof $DLPROF_ARGS | tee -a $INFERENCE_LOG

cp -r ${DLPROF_TMP}* $RUN_DIR/

set +x

