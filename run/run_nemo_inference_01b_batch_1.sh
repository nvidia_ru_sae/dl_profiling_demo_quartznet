#!/usr/bin/env bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup

INFERENCE_ARGS="${INFERENCE_ARGS/--eval_batch_size=[0-9]*/}"
INFERENCE_ARGS+=" --eval_batch_size=1 "

python ./01_speech2text_infer.py $INFERENCE_ARGS | tee -a $INFERENCE_LOG

