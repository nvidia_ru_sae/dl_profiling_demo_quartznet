# Copyright (c) 2019-, NVIDIA CORPORATION. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from argparse import ArgumentParser

import nemo
import nemo.collections.asr as nemo_asr
from nemo.collections.asr.helpers import post_process_predictions, post_process_transcripts, word_error_rate
from nemo.utils import logging

## begining: additional code
import tensorrt as trt
import time
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np
import torch 

def deserialize_engine(engine_path, is_verbose):
    '''Deserializes TRT engine at engine_path
    '''
    TRT_LOGGER = trt.Logger(trt.Logger.VERBOSE) if is_verbose else trt.Logger(trt.Logger.WARNING)
    with open(engine_path, 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
        engine = runtime.deserialize_cuda_engine(f.read())
    return engine

# from common trt samples
def allocate_buffers_with_existing_inputs(context, inp, input_name, output_name):
    '''
    allocate_buffers() (see TRT python samples) but uses an existing inputs on device
    inp:  List of pointers to device memory. Pointers are in the same order as
          would be produced by allocate_buffers(). That is, inputs are in the
          order defined by iterating through `engine`
    '''
    # https://github.com/NVIDIA/DeepLearningExamples/blob/master/PyTorch/SpeechRecognition/Jasper/tensorrt/trtutils.py#L88
    # https://gist.github.com/szagoruyko/440c561f7fce5f1b20e6154d801e6033
    # Add input to bindings
    bindings = [0,0]
    outputs = []
    engine = context.engine
    batch_size = inp[0].shape
    inp_idx = engine.get_binding_index(input_name)    
    inp_b = inp[0].data_ptr()
    assert(inp[0].is_contiguous())
    bindings[inp_idx] = inp_b
    sh = inp[0].shape
    # batch_size = sh[0]
    orig_shape = context.get_binding_shape(inp_idx)
    # if there are issues with seq len, one can dynmically tweak it
    # min_shape, _, max_shape = engine.get_profile_shape(context.active_optimization_profile,inp_idx)
    # sh = np.minimum(sh, max_shape)
    # sh = np.maximum(sh, min_shape)
    if orig_shape[0]==-1:
        context.set_binding_shape(inp_idx, trt.Dims(sh))

    assert context.all_binding_shapes_specified

    out_idx = engine.get_binding_index(output_name)
    # Allocate output buffer by querying the size from the context. This may be different for different input shapes.
    out_shape = context.get_binding_shape(out_idx)
    #print ("Out_shape: ", out_shape)
    d_output = torch.cuda.FloatTensor(*out_shape)
    torch.cuda.synchronize()
    bindings[out_idx] = int(d_output.data_ptr())
    outputs.append(d_output)
    return outputs, bindings, out_shape

def do_inference(context, stream, inp, input_name, output_name):
    '''Do inference using a TRT engine 
    Execution and device-to-host copy are completed synchronously
    '''
    # https://github.com/NVIDIA/DeepLearningExamples/blob/b2e89e6e80ca27b3a030d6419ba13a7022c869f3/PyTorch/SpeechRecognition/Jasper/tensorrt/perfprocedures.py#L126
    # Typical Python-TRT used in samples would copy input data from host to device.
    # Because the PyTorch Tensor is already on the device, such a copy is unneeded.
    
    # Create output buffers and stream
    outputs, bindings, out_shape = allocate_buffers_with_existing_inputs(context, inp, input_name, output_name)
    # simulate sync call here
    context.execute_async_v2(
        bindings=bindings,
        stream_handle=stream.handle)
    stream.synchronize()

    out = outputs[0]
    return out

def monkey_patch_asr_model(asr_model, args, verbose=True, decoder_path="./artifacts/qn_decoder.trt", encoder_path="./artifacts/qn_encoder.trt"):
    jasper_encoder, jasper_decoder = asr_model.modules[2], asr_model.modules[3]
    # https://github.com/NVIDIA/DeepLearningExamples/tree/master/PyTorch/SpeechRecognition/Jasper/tensorrt
    # https://github.com/NVIDIA/DeepLearningExamples/blob/master/PyTorch/SpeechRecognition/Jasper/tensorrt/scripts/inference.sh
    # https://github.com/NVIDIA/DeepLearningExamples/blob/master/PyTorch/SpeechRecognition/Jasper/tensorrt/perf.py
    def wrap_module(module, engine_path, input_name, output_name):
        engine = deserialize_engine(engine_path, verbose)
        # old_forward = module.forward
        stream = cuda.Stream()
        def new_forward(*args, **kwargs):
            # old_result = old_forward(*args, **kwargs)
            with engine.create_execution_context() as context:
                new_result = do_inference(context, stream, [kwargs[input_name]], input_name, output_name)
                if "length" in kwargs.keys():
                    return new_result, kwargs["length"]
                else:
                    return new_result
        module.forward = new_forward
    wrap_module(jasper_decoder, decoder_path, 'encoder_output', 'output')
    wrap_module(jasper_encoder, encoder_path, 'audio_signal', 'outputs') 
    print("patched")


## end: additional code
    



def main():
    # Usage and Command line arguments
    parser = ArgumentParser()
    parser.add_argument(
        "--asr_model",
        type=str,
        default="QuartzNet15x5-En",
        required=True,
        help="Pass: 'QuartzNet15x5-En', 'QuartzNet15x5-Zh', or 'JasperNet10x5-En'",
    )
    parser.add_argument("--dataset", type=str, required=True, help="path to evaluation data")
    parser.add_argument("--eval_batch_size", type=int, default=1, help="batch size to use for evaluation")
    parser.add_argument("--wer_target", type=float, default=None, help="used by test")
    parser.add_argument("--wer_tolerance", type=float, default=1.0, help="used by test")
    parser.add_argument("--trim_silence", default=True, type=bool, help="trim audio from silence or not")
    parser.add_argument(
        "--normalize_text", default=True, type=bool, help="Normalize transcripts or not. Set to False for non-English."
    )

    args = parser.parse_args()

    # Setup NeuralModuleFactory to control training
    # instantiate Neural Factory with supported backend
    nf = nemo.core.NeuralModuleFactory()

    # Instantiate the model which we'll train
    logging.info(f"Speech2Text: Will fine-tune from {args.asr_model}")
    asr_model = nemo_asr.models.ASRConvCTCModel.from_pretrained(model_info=args.asr_model)
    asr_model.eval()

    monkey_patch_asr_model(asr_model, args) ## additional code

    logging.info("\n\n")
    logging.info(f"Evaluation using {type(asr_model)} model.")
    logging.info(f"Evaluation using alphabet {asr_model.vocabulary}.")
    logging.info(f"The model has {asr_model.num_weights} weights.\n\n")

    eval_data_layer = nemo_asr.AudioToTextDataLayer(
        manifest_filepath=args.dataset,
        labels=asr_model.vocabulary,
        batch_size=args.eval_batch_size,
        trim_silence=args.trim_silence,
        shuffle=False,
        normalize_transcripts=args.normalize_text,
    )
    greedy_decoder = nemo_asr.GreedyCTCDecoder()

    audio_signal, audio_signal_len, transcript, transcript_len = eval_data_layer()
    log_probs, encoded_len = asr_model(input_signal=audio_signal, length=audio_signal_len)
    predictions = greedy_decoder(log_probs=log_probs)

    # inference
    eval_tensors = [log_probs, predictions, transcript, transcript_len, encoded_len]
    t0 = time.perf_counter() ## additional code
    evaluated_tensors = nf.infer(tensors=eval_tensors)
    t1 = time.perf_counter() ## additional code


    greedy_hypotheses = post_process_predictions(evaluated_tensors[1], asr_model.vocabulary)
    references = post_process_transcripts(evaluated_tensors[2], evaluated_tensors[3], asr_model.vocabulary)

    if args.asr_model.strip().endswith('-Zh'):
        val = word_error_rate(hypotheses=greedy_hypotheses, references=references, use_cer=True)
        metric = 'CER'
    else:
        val = word_error_rate(hypotheses=greedy_hypotheses, references=references, use_cer=False)
        metric = 'WER'

    t2 = time.perf_counter() ## additional code
    print(f"### Inference itself took {t1 - t0} seconds. {metric} calculation: {t2 - t1}. Total {t2 - t0}") ## additional code
    logging.info(f"Greedy {metric} = {val}")
    if args.wer_target is not None:
        if args.wer_target * args.wer_tolerance < val:
            raise ValueError(f"Resulting {metric} {val} is higher than the target {args.wer_target}")


if __name__ == '__main__':
    main()
