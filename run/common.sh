#!/usr/bin/env bash

INFERENCE_ARGS="--asr_model QuartzNet15x5-En --dataset artifacts/an4/train_manifest.json --eval_batch_size=600"
# where do we store profiles
PROFILES_DIR="/workspace/ext/profiles/"
DLPROF_TMP=/tmp/dlprof/
DLPROF_ARGS="-f true --output_path=${DLPROF_TMP} "

NUM_GPUS=$(nvidia-smi -L | wc -l)

NSYS_ARGS=" --trace cuda,cudnn,nvtx,osrt "


inference_setup() {
    [[ -d "${DLPROF_TMP}" ]] && rm -rf "${DLPROF_TMP}"
    mkdir -p "${DLPROF_TMP}"
    RUN_NAME=`basename "$0"`_${NUM_GPUS}_gpu
    RUN_DIR=$PROFILES_DIR/$RUN_NAME
    echo "### Will store the profiling results in ${RUN_DIR}"
    INFERENCE_LOG=$RUN_DIR/inference.log
    mkdir -p $RUN_DIR
    rm $INFERENCE_LOG
    nvidia-smi -L | tee -a $INFERENCE_LOG
    env | grep CUDA | tee -a $INFERENCE_LOG
    env | grep NVIDIA | tee -a $INFERENCE_LOG

    NSYS_BASE="${DLPROF_TMP}/${RUN_NAME}"
    NSYS_ARGS+=" -o ${NSYS_BASE} --export sqlite"
    DLPROF_ARGS+=" --nsys_database=${NSYS_BASE}.sqlite "
}