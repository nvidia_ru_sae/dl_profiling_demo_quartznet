#!/usr/bin/env python
from argparse import ArgumentParser

import nemo
import nemo.collections.asr as nemo_asr
from nemo.collections.asr.helpers import post_process_predictions, post_process_transcripts, word_error_rate
from nemo.utils import logging
import torch
import yaml

# Usage and Command line arguments
parser = ArgumentParser()
parser.add_argument(
    "--asr_model",
    type=str,
    default="QuartzNet15x5-En",
    help="Pass: 'QuartzNet15x5-En', 'QuartzNet15x5-Zh', or 'JasperNet10x5-En'",
)
parser.add_argument("--eval_batch_size", type=int, default=1, help="batch size to use for evaluation")
parser.add_argument("--onnx", default="artifacts/qn.onnx", type=str, help="Path to the onnx")
args = parser.parse_known_args()[0]

# Setup NeuralModuleFactory to control training
# instantiate Neural Factory with supported backend
nf = nemo.core.NeuralModuleFactory()

# Instantiate the model which we'll train
logging.info(f"Speech2Text: Will fine-tune from {args.asr_model}")
asr_model = nemo_asr.models.ASRConvCTCModel.from_pretrained(model_info=args.asr_model)
asr_model.eval()

jasper_encoder, jasper_decoder = asr_model.modules[2], asr_model.modules[3]

torch.save(jasper_encoder.state_dict(), "artifacts/qn_encoder.pth")
torch.save(jasper_decoder.state_dict(), "artifacts/qn_decoder.pth")