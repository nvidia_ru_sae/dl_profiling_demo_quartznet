#!/usr/bin/env bash

cd /workspace/jasper

./tensorrt/scripts/download_inference_librispeech.sh # downloads data to /datasets/LibriSpeech
./tensorrt/scripts/preprocess_inference_librispeech.sh
mkdir /checkpoints && cd /checkpoints && wget --content-disposition https://api.ngc.nvidia.com/v2/models/nvidia/jasper_pyt_ckpt_amp/versions/19.08.0/zip -O jasper_pyt_ckpt_amp_19.08.0.zip && unzip -o ./jasper_pyt_ckpt_amp_19.08.0.zip
cd /workspace/jasper
./tensorrt/scripts/inference_benchmark.sh 

