#!/usr/bin/env python


# taken from https://github.com/NVIDIA/NeMo/blob/01fcd740f71e75b55c74a6d533c2ce4fea53b506/tutorials/asr/01_ASR_with_NeMo.ipynb
import glob
import os
import subprocess
import tarfile
import wget

data_dir="artifacts/"
os.makedirs(data_dir, exist_ok=True)

# Download the dataset. This will take a few moments...
print("******")
if not os.path.exists(data_dir + '/an4_sphere.tar.gz'):
    an4_url = 'http://www.speech.cs.cmu.edu/databases/an4/an4_sphere.tar.gz'
    an4_path = wget.download(an4_url, data_dir)
    print(f"Dataset downloaded at: {an4_path}")
else:
    print("Tarfile already exists.")
    an4_path = data_dir + '/an4_sphere.tar.gz'

tar = tarfile.open(an4_path)
tar.extractall(path=data_dir)
