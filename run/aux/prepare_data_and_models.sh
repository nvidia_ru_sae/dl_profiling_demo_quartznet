#!/usr/bin/env bash

mkdir -p artifacts

python ./aux/get_an4_data.py
python ../NeMo/scripts/process_an4_data.py --data_root "artifacts"
python ./aux/get_qn_models.py
python ../NeMo/scripts/export_jasper_to_onnx.py --config ../NeMo/examples/asr/configs/quartznet15x5.yaml --nn_encoder "artifacts/qn_encoder.pth" --nn_decoder "artifacts/qn_decoder.pth" --onnx_encoder "artifacts/qn_encoder.onnx" --onnx_decoder "artifacts/qn_decoder.onnx"
head -n 30 artifacts/an4/test_manifest.json | tee artifacts/an4/minimal_test_manifest.json