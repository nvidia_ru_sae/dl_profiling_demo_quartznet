#!/bin/bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup

DLPROF_ARGS+=" --key_node encoder " 
CMD="dlprof ${DLPROF_ARGS} python ${CUR_DIR}./04_speech2text_infer_tensorrt_nvtx.py $INFERENCE_ARGS | tee -a $INFERENCE_LOG"
eval "$CMD"


cp -r ${DLPROF_TMP}* $RUN_DIR/

