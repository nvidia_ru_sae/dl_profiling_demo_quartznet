#!/usr/bin/env bash

CUR_DIR=`dirname "$0"`/
source ${CUR_DIR}./common.sh

inference_setup

python ./03_speech2text_infer_tensorrt.py $INFERENCE_ARGS | tee -a $INFERENCE_LOG

