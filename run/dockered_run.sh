#!/usr/bin/env bash
[[ -z $1 ]] && echo "specify the executable name" && exit 1

../profiling-compose/docker-run.sh /bin/bash \
"-c \"cd /workspace/ext/run; ${1}\""