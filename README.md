# Profiling demo notes

NeMo folder contains mostly original NeMo master branch clone.
```bash
git clone https://github.com/NVIDIA/NeMo.git
git checkout v0.11.1
```
I use newer PyTorch NGC container as a base to get the new profiler features, so had to modify the Dockerfile a bit.

`./profiling-compose` contains a pipeline for building a docker image.

`./run/export_to_onnx.ipynb` contains instructions to download a pre-trained NeMo Quartznet model and then exoport it to trt via onnx. Another cell downloads an4 dataset, used for evaluation.
`./run` contains all the scripts used to run the demo
The idea is to have a common setup in `./run/common.sh` and have special `./run/run_*.sh` scripts for every other run, so that everything is easily reproducible.

`./run/01_speech2text_infer.py` is a copy of `NeMo/examples/asr/speech2text_infer.py` with minimal additional code for timing. In each file additional code is marked with `##` and `additional code` comments.

`./run/speech2text_infer_pyprof.py` is the same file, where pyprof init directives are added.

```python
import torch.cuda.profiler as profiler
import pyprof
pyprof.init(enable_function_stack=True)
```


